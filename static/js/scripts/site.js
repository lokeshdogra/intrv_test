var is_touch = (typeof Touch == "object");

(function() {
  var resize_timer;

  var d,
      w,
      header_strip,
      header,
      tracer;

  var page,
      mode,
      modes = {
        1: 'large',
        2: 'medium',
        3: 'small'
      };

  var banner_heights = {
        'large' : 366,
        'medium': 306,
        'small': 241
      };

  var BANNER_WIDTH = 1488,
      BANNER_HEIGHT = 330,
      CONDENSED_BANNER_HEIGHT = 135;

  var logo_visible = true;

  var kickstart = function() {
    var width = parseInt(tracer.css('width'), 10);
    if (width == 0) {
      setTimeout(kickstart, 50);
    }
    else {
      init();
    }
  }

  var init = function() {
    d = $(document);
    w = $(window);
    header_strip = $('#header-strip');
    header = $('#header');
    page = $('body').attr('id');

    if (page == 'home') logo_visible = false;

    w.resize(function() {
      clearTimeout(resize_timer);
      resize_timer = setTimeout(resize, 250);
    });
    resize();

    w.scroll(scroll);
  }

  var resize = function() {
    var old_mode = mode;
    mode = modes[parseInt(tracer.css('width'), 10)];


    if (old_mode != mode) {
      $(document).trigger('splendid.size_change', mode);
    };

    var w_width = document.documentElement.clientWidth;

    if (BANNER_WIDTH < w_width) {
      b_width = w_width;
    }
    else {
      b_width = BANNER_WIDTH;
    }

    var b_height = BANNER_HEIGHT;

    if (mode == 'medium') {
      b_height = banner_heights['medium'] - 36;
      b_width = b_height * BANNER_WIDTH / BANNER_HEIGHT;
    }

    if (mode == 'small') {
      b_height = banner_heights['small'] - 36;
      b_width = b_height * BANNER_WIDTH / BANNER_HEIGHT;
    }

    if (b_width > BANNER_WIDTH) {
      b_height = b_width * BANNER_HEIGHT / BANNER_WIDTH;
    }

    $('#header-strip-image img').css({ 
      left: px((w_width - b_width) / 2),
      width: px(b_width),
      height: px(b_height)
    });
    scroll();
  }

  var scroll = function() {
    if (is_touch) return;

    if (mode == 'small') {
      header_strip.css({ height: px(banner_heights['small'] - 36) }); 
      header.css({ height: px(banner_heights['small'] - 36)});
      return; 
    }
    var offset = w.scrollTop();
    
    if (offset < 0) offset = 0;
    var h = banner_heights[mode];
    var new_height = h - offset;
    if (new_height < CONDENSED_BANNER_HEIGHT) new_height = CONDENSED_BANNER_HEIGHT;

    header_strip.css({ height: px(new_height) }); 
    header.css({ height: px(new_height - 36)}); 

    var logo_was_visible = logo_visible;
    logo_visible = (new_height == CONDENSED_BANNER_HEIGHT);

    if (page == 'home' && logo_was_visible != logo_visible) {
      opacity = (logo_was_visible) ? 0 : 1;
      $('h1').stop(true, false).animate({ opacity: opacity }, 500);
    }
  }

  // Bindings

  $(function() {
    tracer = $('#tracer');
    kickstart();
    
    var email_form = $('#email-form');
    
    email_form.find('span').on('click', function() {
      email_form.submit();
    });
    
    email_form.on('submit', function() {
      var email = email_form.find('input').val();
      var data = { 'cm-yhktn-yhktn' : email };
      $.ajax({
        type: 'POST',
        url: 'http://somethingsplendid.createsend.com/t/r/s/yhktn/',
        data: data
      });
      
      email_form.find('p.signup').animate({ opacity: 0 }, 500, function() {
        $(this).remove();
        email_form.find('p.thanks').css({ opacity: 0, display: 'block' }).animate({ opacity: 1 }, 500);
      })
      return false;
    });
    
  });

})();

// Here be helpers

var px = function(x) { return x + 'px' }
 
function relative_time(t) {
  /*
   * JavaScript Pretty Date
   * Copyright (c) 2008 John Resig (jquery.com)
   * Licensed under the MIT license.
   */

  time = t;
  
  var date = new Date(time),
    diff = (((new Date()).getTime() - date.getTime()) / 1000),
    day_diff = Math.floor(diff / 86400);

  if ( isNaN(day_diff) || day_diff < 0 )
    return;
          
  return day_diff == 0 && (
      diff < 60 && "just now" ||
      diff < 120 && "1 minute ago" ||
      diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
      diff < 7200 && "1 hour ago" ||
      diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
    day_diff == 1 && "Yesterday" ||
    day_diff < 7 && day_diff + " days ago" ||
    Math.ceil( day_diff / 7 ) + " weeks ago";
}

