var Layouter = function(w, nc, cw) {
  var methods = {},
      columns = [], 
      gaps = [],
      wrapper = w,
      num_columns = nc,
      column_width = cw;
      
  var init = function() {
    clear_blocks();
  }
  
  var set_columns = function(new_columns) {
    num_columns = new_columns;
  }
  
  var set_column_width = function(new_column_width) {
    column_width = new_column_width;
  }
  
  var clear_blocks = function() {
    add_course(0);
  }
  
  var height = function() {
    return Math.max.apply(Math, columns);
  }
  
  var add_course = function(h) {
    columns = [];
    gaps = [];
    for (var i = 0; i < num_columns; i++) columns[i] = h;
  } 
  
  var stack = function(blocks, place_function) {
    blocks.each(function() {
      var block = $(this),
          block_height = block.outerHeight(true),
          block_width = block.outerWidth(true),
          block_columns = Math.ceil(block_width / column_width);
          
      var min_top = 1e5,
          min_column,
          gap_found = false;
          
      var new_top,
          new_left;
          
          
      // Is this a one column block? If so, attempt to fill gap
      if (block_columns == 1) {
        for (var i = 0; i < gaps.length; i++) {
          var gap = gaps[i];
          if (gap.height >= block_height) {
            gap_found = true;
            
            new_top = gap.position;
            new_left = gap.column * column_width;
            
            var new_gap = {
              column: gap.column,
              height: gap.height - block_height,
              position: gap.position + block_height
            };
            
            gaps[i] = new_gap;
            break;
          }
        }
      }
      
      if (!gap_found) {
        // Sweep l-r for a spot wide enough for our block;
        // Save the spot closest to the top of the page 
        for (var i = 0; i < num_columns + 1 - block_columns; i++) {
          var lowest = Math.max.apply(Math, columns.slice(i, i + block_columns));
          if (lowest < min_top) {
            min_column = i;
            min_top = lowest;
          }
        }
        
        // Update column heights based on new block position
        for (var i = min_column; i < min_column + block_columns; i++) {
          var old_height = columns[i],
              new_height = min_top + block_height,
              gap_height = new_height - old_height - block_height;
              
          // If a gap was left in this column, remember it
          if (gap_height) {
            var new_gap = {
              column: i,
              height: gap_height,
              position: old_height
            };
            gaps.push(new_gap);
          }
          columns[i] = new_height;
        }
        
        new_top = min_top;
        new_left = min_column * column_width;
      }
      
      if (typeof place_function == 'function') {
        place_function(block, new_top, new_left);
      }
      else {
        place_block(block, new_top, new_left);
      }
      
    });

    // Update wrapper to match width and height of stacked blocks
    wrapper.css({
      height: px(height()),
      width: px(num_columns * column_width)
    });
  }
  
  var place_block = function(block, top, left) {
    block.css({
      top: px(top),
      left: px(left),
      position: 'absolute'
    });
  }
  
  init();
  
  methods.set_columns = set_columns;
  methods.set_column_width = set_column_width;
  methods.clear_blocks = clear_blocks;
  methods.stack = stack;
  methods.height = height;
  methods.add_course = add_course;
  
  return methods;
}